require 'rails_helper'

RSpec.describe V1::SessionsController, type: :controller do
  describe '#create' do
    it 'should return 201' do
      VCR.use_cassette('session-login-success') do
        post :create, params: { email: 'john@doe.com.tr', password: 'whoooaaa' }
      end

      expect(response.status).to eq(201)
    end

    it 'should return json data' do
      VCR.use_cassette('session-login-success') do
        post :create, params: { email: 'john@doe.com.tr', password: 'whoooaaa' }
      end

      expect { JSON.parse(response.body) }.not_to raise_error
    end

    it 'should return authorization token' do
      VCR.use_cassette('session-login-success') do
        post :create, params: { email: 'john@doe.com.tr', password: 'whoooaaa' }
      end

      parsed_response = JSON.parse(response.body)

      expect(parsed_response['data']['access_token'].present?).to eq(true)
    end
  end

  describe '#destroy' do
    it 'should return 204' do
      @request.set_header 'AUTH_TOKEN', 'blah'

      VCR.use_cassette('session-login-destroy') do
        delete :destroy
      end

      expect(response.status).to eq(204)
    end

    it 'should return empty body' do
      @request.set_header 'AUTH_TOKEN', 'blah'

      VCR.use_cassette('session-login-destroy') do
        delete :destroy
      end

      expect(response.body.present?).to eq(false)
    end

    it 'should revoke token access' do
    end
  end
end
