require 'rails_helper'

RSpec.describe V1::WidgetsController, type: :controller do
  describe '#index' do
    it 'should return 200' do
      VCR.use_cassette('widget-api-public-widgets-index') do
        get :index
      end

      expect(response.status).to eq(200)
    end

    it 'should not return result' do
      VCR.use_cassette('widget-api-public-widgets-index-filtered') do
        get :index, params: { q: 'coo00000aslkdnajkdal'}
      end

      res = JSON.parse(response.body)

      expect(res['data'].size).to eq(0)
    end
  end
end
