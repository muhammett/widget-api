require 'rails_helper'

RSpec.describe V1::UsersController, type: :controller do
  describe '#create' do

    context 'with valid data' do
      it 'should return 201' do
        VCR.use_cassette('widget-api-user-creation-valid') do
          post :create, params: { first_name: 'John',
                                  last_name: 'Doe',
                                  password: 'whoooaaa',
                                  email: 'john@doe.com.tr' }
        end

        expect(response.status).to eq(201)
      end

      it 'should return access_token' do
        VCR.use_cassette('widget-api-user-creation-valid') do
          post :create, params: { first_name: 'John',
                                  last_name: 'Doe',
                                  password: 'whoooaaa',
                                  email: 'john@doe.com.tr' }
        end

        resp = JSON.parse(response.body)

        puts resp

        expect(resp['data']['access_token'].present?).to eq(true)
      end
    end

    context 'with invalid data' do
      it 'should return 422' do
        VCR.use_cassette('widget-api-user-creation-invalid') do
          post :create
        end

        expect(response.status).to eq(422)
      end

      it 'should return erros' do
        VCR.use_cassette('widget-api-user-creation-invalid') do
          post :create
        end

        resp = JSON.parse(response.body)

        expect(resp['errors'].present?).to eq(true)
      end
    end
  end

  # describe '#update' do
  #   it 'should return 200' do
  #     put :update, params: { id: 'me' },
  #                  body: { first_name: 'John',
  #                          last_name: 'Doe',
  #                          date_of_birth: '1464083530'}
  #
  #     expect(response.status).to eq(200)
  #   end
  #
  #   it 'should not update others' do
  #     put :update, params: { id: '1' },
  #                  body: { first_name: 'John',
  #                          last_name: 'Doe',
  #                          date_of_birth: '1464083530'}
  #
  #     expect(response.status).to eq(400)
  #   end
  # end
  #
  # describe '#show' do
  #   it 'should return 200' do
  #   end
  # end
end
