module WidgetApiWrapper
  ENDPOINT = 'https://showoff-rails-react-production.herokuapp.com/api/v1'.freeze

  AUTH_ENDPOINT = 'https://showoff-rails-react-production.herokuapp.com/oauth'.freeze

  class Current < ActiveSupport::CurrentAttributes
    attribute :authorization_token
  end
end
