# frozen_string_literal: true

module WidgetApiWrapper
  class Widget < Base

    attr_accessor :id, :name, :kind, :user, :owner, :description, :errors

    def initialize(args = {})

      args = args.with_indifferent_access

      self.id           = args.delete(:id)
      self.name         = args.delete(:name)
      self.description  = args.delete(:description)
      self.kind         = args.delete(:kind)
      self.user         = args.delete(:user)
      self.owner        = args.delete(:owner)
      self.errors       = []
    end

    def save
      id.present? ? update : create
    end

    def destroy
      fetcher = WidgetApiWrapper::Fetcher.new("/widgets/#{id}",:delete)

      fetcher.run

      return true if fetcher.code == 200

      errors << fetcher.response['message']

      false
    end

    class << self
      def all(query = nil)

        fetcher = WidgetApiWrapper::Fetcher.new('/widgets/visible',
                                                :get,
                                                params: { term: query })

        fetcher.run

        return [] unless fetcher.code == 200

        fetcher.response['data']['widgets'].map do |item|

          widget = self.new

          widget.fill_data(item)

          widget
        end
      end
    end

    def fill_data_from_response(fetcher)
      if fetcher.code == 200
        fill_data(fetcher.response['data']['widget'])
      else
        fill_error(fetcher.response['message'])
      end
    end

    def fill_data(data)
      user_item = data['user']

      self.user         = WidgetApiWrapper::User.new(user_item)

      self.id           = data['id']
      self.owner        = data['owner']
      self.name         = data['name']
      self.description  = data['description']
      self.kind         = data['kind']
    end

    def fill_error(error)
      errors << error
    end

    private

    def create
      widget = { name: name, kind: kind, description: description }

      fetcher = WidgetApiWrapper::Fetcher.new('/widgets',
                                              :post,
                                              body: { widget: widget })

      fetcher.run

      fill_data_from_response(fetcher)

      fetcher.code == 200
    end

    def update
      widget = { name: name, kind: kind, description: description }

      fetcher = WidgetApiWrapper::Fetcher.new("/widgets/#{id}",
                                              :post,
                                              body: { widget: widget })

      fetcher.run

      fill_data_from_response(fetcher)

      fetcher.code == 200
    end
  end
end
