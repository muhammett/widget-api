# frozen_string_literal: true

module WidgetApiWrapper
  class User < Base

    attr_accessor :id,
                  :first_name,
                  :last_name,
                  :password,
                  :email,
                  :date_of_birth,
                  :profile_pic,
                  :access_token,
                  :errors


    def initialize(args = {})

      args = args.with_indifferent_access

      images = args.delete(:images)

      self.id             = args.delete(:id)
      self.first_name     = args.delete(:first_name)
      self.last_name      = args.delete(:last_name)
      self.email          = args.delete(:email)
      self.date_of_birth  = args.delete(:date_of_birth)
      self.password       = args.delete(:password)

      self.profile_pic = if images.present?
                           images['medium_url']
                         else
                           args.delete(:profile_pic)
                         end

      self.access_token   = args.delete(:access_token)
      self.errors         = []
    end

    def save
      id.present? ? update : create
    end

    def widgets(query = nil)
      return [] unless id.present?

      fetcher = WidgetApiWrapper::Fetcher.new("/users/#{id}/widgets",
                                              :get,
                                              params: { term: query })

      fetcher.run

      return [] unless fetcher.code == 200

      fetcher.response['data']['widgets'].map do |item|
        widget = WidgetApiWrapper::Widget.new

        widget.fill_data(item)

        widget
      end
    end

    class << self
      def find_by_id(id)
        fetcher = WidgetApiWrapper::Fetcher.new("/users/#{id}", :get)

        fetcher.run

        return nil unless fetcher.code == 200

        data = fetcher.response['data']['user']

        WidgetApiWrapper::User.new(data)
      end
    end

    private

    def create
      user = { first_name: first_name,
               last_name: last_name,
               password: password,
               email: email,
               image_url: 'https://static.thenounproject.com/png/961-200.png' }


      fetcher = WidgetApiWrapper::Fetcher.new('/users',
                                              :post,
                                              body: { user: user })

      fetcher.run

      if fetcher.code == 200
        self.access_token = fetcher.response['data']['token']['access_token']
        self.id           = fetcher.response['data']['user']['id']
        true
      else
        errors << fetcher.response['message']

        false
      end
    end

    def update
      user = { first_name: first_name,
               last_name: last_name,
               date_of_birth: date_of_birth,
               image_url: 'https://static.thenounproject.com/png/961-200.png' }

      fetcher = WidgetApiWrapper::Fetcher.new '/users/me',
                                              :put,
                                              body: { user: user }

      fetcher.run

      if fetcher.code == 200
        true
      else
        errors << fetcher.response['message']

        false
      end
    end
  end
end