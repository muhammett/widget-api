# frozen_string_literal: true

module WidgetApiWrapper
  class Sessions < Base

    attr_accessor :username,
                  :password,
                  :access_token,
                  :refresh_token,
                  :expires_in,
                  :errors

    def initialize(args = {})
      self.username     = args.delete(:username)
      self.password     = args.delete(:password)
      self.errors       = []
    end

    def save
      body = { grant_type: 'password',
               username: username,
               password: password }

      fetcher = WidgetApiWrapper::Fetcher.new('/token',
                                              :post,
                                              auth: true,
                                              body: body)

      fetcher.run

      unless fetcher.code == 200
        errors << fetcher.response['message']
        return false
      end

      self.password       = nil
      self.access_token   = fetcher.response['data']['token']['access_token']
      self.refresh_token  = fetcher.response['data']['token']['refresh_token']
      self.expires_in     = fetcher.response['data']['token']['expires_in']

      true
    end
  end
end
