# frozen_string_literal: true

module  WidgetApiWrapper
  class Fetcher

    attr_reader :code, :response

    def initialize(path, method, args = {})
      @url = args.delete(:auth).present? ? AUTH_ENDPOINT : ENDPOINT

      @url += path

      @method = method

      @body   = args.delete(:body).presence || {}

      @body[:client_id]       = ENV['W_CLIENT_ID']
      @body[:client_secret]   = ENV['W_CLIENT_SECRET']

      @params = args.delete(:params).presence || {}

      @headers = args.delete(:headers).presence || {}

      @headers[:params] = @params

      @headers['Content-Type'] = 'application/json'

      if WidgetApiWrapper::Current.authorization_token.present?
        @headers['Authorization'] = "Bearer #{WidgetApiWrapper::Current.authorization_token}"
      end
    end

    def run
      response = RestClient::Request.execute(method: @method,
                                             url: @url,
                                             payload: @body.to_json,
                                             headers: @headers)
    rescue StandardError => err
      @code     = err.response.code
      @response = JSON.parse(err.response.body)
    else
      @code = response.code
      @response = JSON.parse(response.body)
    end
  end
end