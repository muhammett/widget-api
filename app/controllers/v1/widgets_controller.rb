# frozen_string_literal: true

module V1
  class WidgetsController < Base

    # List public widgets
    def index
      render_data(200, WidgetApiWrapper::Widget.all(params[:q]))
    end

    def create
      widget = WidgetApiWrapper::Widget.new

      widget.name         = params[:name]
      widget.kind         = params[:kind]
      widget.description  = params[:description]

      if widget.save
        render_data(201, widget.as_json)
      else
        render_error(422, widget.errors.as_json)
      end
    end

    def update
      widget = WidgetApiWrapper::Widget.new

      widget.id           = params[:id]
      widget.name         = params[:name]
      widget.kind         = params[:kind]
      widget.description  = params[:description]

      if widget.save
        render_data(200, widget.as_json)
      else
        render_error(422, widget.errors.as_json)
      end
    end

    def destroy
      widget = WidgetApiWrapper::Widget.new

      widget.id = params[:id]

      if widget.destroy
        render_data(200, nil)
      else
        render_error(422, widget.errors.as_json)
      end
    end
  end
end
