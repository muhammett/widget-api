# frozen_string_literal: true

module V1
  class UsersController < Base
    def create
      user = WidgetApiWrapper::User.new

      user.first_name = params[:first_name]
      user.last_name  = params[:last_name]
      user.email      = params[:email]
      user.password   = params[:password]

      user.save ? render_data(201, user.as_json) : render_error(422, user.errors.as_json)
    end

    def update; end

    def show
      user = WidgetApiWrapper::User.find_by_id(params[:id])

      if user.present?
        render_data(200, user.as_json)
      else
        render_data(404, nil)
      end
    end
  end
end
