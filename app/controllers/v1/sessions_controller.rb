# frozen_string_literal: true

module V1
  class SessionsController < Base
    def create
      session = WidgetApiWrapper::Sessions.new(username: params[:email],
                                               password: params[:password])

      if session.save
        render_data(201, session.as_json)
      else
        render_error(401, session.errors.as_json)
      end
    end

    def destroy; end
  end
end
