# frozen_string_literal: true

module V1
  module Users
    class WidgetsController < Base
      def index
        user = WidgetApiWrapper::User.new

        user.id = params[:user_id]

        render_data(200, user.widgets(params[:q]))
      end
    end
  end
end
