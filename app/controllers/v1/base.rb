# frozen_string_literal: true

module V1
  class Base < ApplicationController
    before_action :set_current_token

    def render_data(code, data)
      render json: { code: code, data: data }, status: code
    end

    def render_error(code, errors)
      render json: { code: code, errors: errors }, status: code
    end

    def set_current_token
      WidgetApiWrapper::Current.authorization_token = request.headers['Authorization']
    end
  end
end
