Rails.application.routes.draw do
  namespace :v1, defaults: { format: 'json' } do
    resources :sessions, only: %i[create] do
      delete :destroy, on: :collection
    end

    resources :users, except: :destroy do
      scope module: :users do
        resources :widgets, only: [:index]
      end
    end

    resources :widgets, only: %i[index create update destroy]
  end
end
