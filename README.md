# Installation

* Clone this repository
* cd widget-api
* bundle install

* Set `W_CLIENT_ID` as an environment variable.
* Set `W_CLIENT_SECRET` as an environment variable.

* Make sure that env variables are set.

* Run `rails s webrick -p 3001`

# Demo

http://showoff-api.muhammet.dev